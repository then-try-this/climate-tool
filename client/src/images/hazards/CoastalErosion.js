import * as React from "react";
const SvgCoastalErosion = (props) => (
<svg height="116.62866" id="svg1933" width="116.62866" version="1.1" viewBox="0 0 30.858 30.858" xmlns="http://www.w3.org/2000/svg">
	<defs id="defs1927">
		<mask id="mask2260" maskUnits="userSpaceOnUse">
			<rect height="109.59391" id="rect2262" style={{"opacity":"1","fill":"#ffffff","fillOpacity":"1","stroke":"none","strokeWidth":"0.56499994","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="86.248459" x="10.399542" y="-2.1886201"/>
		</mask>
		<mask id="mask2260-9" maskUnits="userSpaceOnUse">
			<rect height="109.59391" id="rect2262-6" style={{"opacity":"1","fill":"#ffffff","fillOpacity":"1","stroke":"none","strokeWidth":"0.56499994","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="86.248459" x="10.399542" y="-2.1886201"/>
		</mask>
		<mask id="mask2260-7" maskUnits="userSpaceOnUse">
			<rect height="109.59391" id="rect2262-8" style={{"opacity":"1","fill":"#ffffff","fillOpacity":"1","stroke":"none","strokeWidth":"0.56499994","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="86.248459" x="10.399542" y="-2.1886201"/>
		</mask>
		<mask id="mask2260-9-5" maskUnits="userSpaceOnUse">
			<rect height="109.59391" id="rect2262-6-5" style={{"opacity":"1","fill":"#ffffff","fillOpacity":"1","stroke":"none","strokeWidth":"0.56499994","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} width="86.248459" x="10.399542" y="-2.1886201"/>
		</mask>

	</defs>
	<g id="layer1" style={{"display":"inline"}} transform="translate(0,-266.14199)">
		<circle id="path853-5" style={{"opacity":"1","fill":"#b95b09","fillOpacity":"1","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} cx="15.429177" cy="281.57117" r="15.429177"/>
		<circle id="path853" style={{"opacity":"1","fill":"#031d44","fillOpacity":"1","stroke":"none","strokeWidth":"0.5","strokeMiterlimit":"4","strokeDasharray":"none","strokeOpacity":"1"}} cx="15.429177" cy="281.57117" r="0"/>
		<g id="layer2-7" style={{"display":"inline"}} transform="translate(-0.05041118,265.87472)">
			<rect height="26.819483" id="rect31121" style={{"clipRule":"evenodd","fill":"none","fillRule":"evenodd","strokeWidth":"0.26458335","strokeLinejoin":"round","strokeMiterlimit":"2"}} width="26.819483" x="5.220048" y="5.7397647"/>
			<g id="g42384" style={{"fill":"#ffffff","strokeWidth":"18.944561"}} transform="matrix(0.01862159,0,0,0.01862159,4.4351821,3.820196)">
				<path id="path42358" style={{"fill":"#ffffff","strokeWidth":"18.944561"}} d="m 300,150 c 0,-30.938 -25.312,-56.25 -56.25,-56.25 h -37.5 v 37.5 h 37.5 c 10.312,0 18.75,8.4375 18.75,18.75 0,10.3125 -8.4375,18.75 -18.75,18.75 H 37.5 v 37.5 H 243.75 C 274.688,206.25 300,180.938 300,150 Z"/>
				<path id="path42360" style={{"fill":"#ffffff","strokeWidth":"18.944561"}} d="m 150,281.25 h 243.75 c 10.312,0 18.75,8.4375 18.75,18.75 0,10.3125 -8.4375,18.75 -18.75,18.75 h -37.5 v 37.5 h 37.5 c 31.172,0 56.25,-25.078 56.25,-56.25 0,-31.172 -25.078,-56.25 -56.25,-56.25 H 150 Z"/>
				<path id="path42362" style={{"fill":"#ffffff","strokeWidth":"18.944561"}} d="m 243.75,525 h 37.5 v 37.5 h -37.5 z"/>
				<path id="path42364" style={{"fill":"#ffffff","strokeWidth":"18.944561"}} d="M 187.5,450 H 225 v 37.5 h -37.5 z"/>
				<path id="path42366" style={{"fill":"#ffffff","strokeWidth":"18.944561"}} d="m 206.25,618.75 h 37.5 v 37.5 h -37.5 z"/>
				<path id="path42368" style={{"fill":"#ffffff","strokeWidth":"18.944561"}} d="m 300,412.5 h 37.5 V 450 H 300 Z"/>
				<path id="path42370" style={{"fill":"#ffffff","strokeWidth":"18.944561"}} d="m 420.7,1001.2 70.547,-13.125 12.891,-77.578 -50.391,-25.312 -56.719,56.719 z"/>
				<path id="path42372" style={{"fill":"#ffffff","strokeWidth":"18.944561"}} d="M 223.83,1038 197.346,1011.281 75.006,990.89 v 74.062 z"/>
				<path id="path42374" style={{"fill":"#ffffff","strokeWidth":"18.944561"}} d="m 187.5,971.72 v -38.438 l -46.172,-11.484 -19.453,38.906 z"/>
				<path id="path42376" style={{"fill":"#ffffff","strokeWidth":"18.944561"}} d="m 237.89,998.91 30.938,30.938 31.172,-5.8594 v -22.5 l -22.5,-22.5 z"/>
				<path id="path42378" style={{"fill":"#ffffff","strokeWidth":"18.944561"}} d="m 225,904.22 v 59.297 l 60,-30 52.5,52.5 v 30.938 l 45.703,-8.6719 -30.234,-75.234 93.281,-93.281 99.375,49.688 -15.234,91.172 20.625,-3.9844 67.5,-101.25 v -114.61 l 52.266,-52.266 -36.328,-72.656 37.5,-56.25 -31.172,-62.344 c -32.344,-31.406 -75.469,-48.984 -120.47,-48.75 h -11.484 c -79.922,0.23438 -148.59,56.484 -164.3,134.77 l -0.46875,2.1094 c -1.1719,5.8594 2.5781,11.484 8.4375,12.656 0.70312,0.23438 1.4062,0.23438 2.1094,0.23438 h 2.3438 c 4.4531,0 8.6719,-2.3438 10.781,-6.0938 l 35.156,-58.828 c 10.781,-17.812 30,-28.828 50.859,-28.828 25.547,0 48.281,16.406 56.25,40.547 40.781,75.938 12.188,170.86 -63.75,211.64 -11.953,6.3281 -24.609,11.25 -37.734,14.297 l -217.27,51.797 -8.6719,-36.562 217.27,-51.797 c 36.328,-8.4375 66.328,-33.516 81.328,-67.734 5.625,-12.656 8.9062,-26.484 9.8438,-40.312 -26.719,43.359 -71.719,72.188 -122.11,78.516 l -293.91,41.484 v 204.14 l 46.172,-92.344 z"/>
				<path id="path42380" style={{"fill":"#ffffff","strokeWidth":"18.944561"}} d="m 677.81,638.91 38.672,77.344 -60.234,60.234 v 110.39 l -82.5,123.75 -307.73,57.656 -191.02,34.922 v 21.797 h 1050 v -487.5 H 678.748 Z M 562.5,1087.5 H 525 V 1050 h 37.5 z m 150,-37.5 H 675 v -37.5 h 37.5 z m 187.5,18.75 h -37.5 v -37.5 H 900 Z m 150,18.75 h -37.5 V 1050 h 37.5 z m -18.75,-300 h 37.5 V 825 h -37.5 z M 975,693.75 h 37.5 v 37.5 H 975 Z M 975,900 h 37.5 v 37.5 H 975 Z M 881.25,750 h 37.5 v 37.5 h -37.5 z M 862.5,993.75 H 825 v -37.5 h 37.5 z M 843.75,900 h -37.5 v -37.5 h 37.5 z M 787.5,675 H 825 v 37.5 H 787.5 Z M 750,768.75 h 37.5 v 37.5 H 750 Z m -18.75,150 h -37.5 v -37.5 h 37.5 z"/>
				<path id="path42382" style={{"fill":"#ffffff","strokeWidth":"18.944561"}} d="M 1125,525 V 318.75 L 993.75,187.5 862.5,318.75 V 525 H 750 V 412.03 c 42.656,-4.4531 75,-40.312 75,-82.969 0,-6.7969 -0.9375,-13.594 -2.5781,-20.156 l -16.172,-65.156 v -93.75 c 0,-41.484 -33.516,-75 -75,-75 -41.484,0 -75,33.516 -75,75 v 93.75 l -16.172,64.922 c -1.6406,6.5625 -2.5781,13.359 -2.5781,20.156 0,42.891 32.344,78.75 75,82.969 v 113.2 h -26.016 l 28.828,57.656 -11.719,17.344 h 421.41 z m -168.75,0 V 375 h 75 v 150 z"/>
			</g>
		</g>
	</g>
	<g id="layer2" style={{"display":"inline"}}/>
</svg>
);
export default SvgCoastalErosion;

